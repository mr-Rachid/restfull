<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">RESTfull APIs Project </h1>
    <br>
</p>
![alte text](/usersget.PNG)

im using Yii 2 Basic Template in this project.

The template contains the basic features including user login/logout and a contact page.
It includes all commonly used configurations that would allow you to focus on adding new
features to your application.

REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.6.0. and Mysql5.7 


INSTALLATION
------------

### Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

clone the project with git:

~~~
git clone https://gitlab.com/mr-Rachid/restfull.git
~~~

go to the root folder:`cd RESTfull`

then install this project using the following command: `composer install`

Now you should be able to access the application through the following URL, assuming `RESTfull` is the directory
directly under the Web root.

You can then access the application through the following URL:

~~~
http://localhost/RESTfull/web/
~~~
or go to root folder and run `php yii serve`  
~~~
http://localhost:8080
~~~

RESTfull APIs
-------------
users:
------
get users 
```
curl -i -H "Accept:application/json" "http://localhost:8080/users/" 

```
get user where id=1  
```
curl -i -H "Accept:application/json" "http://localhost:8080/users/1" 
```
products:
---------
GET products 
```
curl -i -H "Accept:application/json" "http://localhost:8080/products/" 
```

GET product where id =2 
```
curl -i -H "Accept:application/json" "http://localhost:8080/products/2" 
```

Create a new Product 
```
curl -i -H "Accept:application/json" -H "Content-Type:application/json"  -XPOST "http://localhost:8080/products" \  -d '{"name":"smartphone", "price":"1000", "status":"1","description":"just a smart phone !"}'
```

Update product where id =5
```
curl -i -H "Accept:application/json" -H "Content-Type:application/json"  -XPUT "http://localhost:8080/products/5" \  -d '{"name":"smartphone", "price":"1200", "status":"1","description":"just a new smartphone !"}'
```

Delete product where id = 5
```
curl -i -H "Accept:application/json" -H "Content-Type:application/json"  -XDELETE "http://localhost:8080/products/5" 
```

![alte text](/productpost.PNG)
![alte text](/productput.PNG)
![alte text](/resfull_Apis.PNG)


CONFIGURATION
-------------

### Database
Create your DB from ecommerce.sql file.   
Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=ecommerce',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];
```

**NOTES:**
- Yii won't create the database for you, this has to be done manually before you can access it.
- Check and edit the other files in the `config/` directory to customize your application as required.
- Refer to the README in the `tests` directory for information specific to basic application tests.


TESTING
-------

Tests are located in `tests` directory. They are developed with [Codeception PHP Testing Framework](http://codeception.com/).
By default there are 3 test suites:

- `unit`
- `functional`
- `acceptance`

Tests can be executed by running

```
vendor/bin/codecept run
```

The command above will execute unit and functional tests. Unit tests are testing the system components, while functional
tests are for testing user interaction. Acceptance tests are disabled by default as they require additional setup since
they perform testing in real browser. 
